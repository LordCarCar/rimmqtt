/*
 * uart.c
 *
 *  Created on: 25 окт. 2020 г.
 *      Author: Lord
 */

#include <user_interface.h>

#include "driver/uart_register.h"
#include "driver/uart.h"

uart_cb_t uart_cb = NULL;

void uart_intr_handler(void *param) {
	RcvMsgBuff *rx_buf = (RcvMsgBuff*) param;
	if (READ_PERI_REG(UART_INT_ST(UART0)) & UART_RXFIFO_FULL_INT_ST != 0) {
		do {
			uint8 ch = READ_PERI_REG(UART_FIFO(UART0));
			int cnt = rx_buf->pWritePos - rx_buf->pReadPos;
			if (cnt < 0)
				cnt += rx_buf->RcvBuffSize;
			if (ch == '\b') {
				if (cnt > 0) {
					uint8 *pWritePos = rx_buf->pWritePos - 1;
					if (pWritePos < rx_buf->pRcvMsgBuff)
						pWritePos += rx_buf->RcvBuffSize;
					if (*pWritePos != '\r') {
						rx_buf->pWritePos = pWritePos;
//						WRITE_PERI_REG(UART_FIFO(UART0), ch);
					}
				}
				break;
			}
			if (cnt < rx_buf->RcvBuffSize - 1) {
				if (ch == '\r' || ch == '\n') {
					static uint32 crlf = 0;
					crlf |= ch == '\r' ? 0x01 : 0x02;
					if (crlf == 0x03) {
						crlf = 0;
						break;
					}
					ch = '\r';
					rx_buf->BuffState++;
//					WRITE_PERI_REG(UART_FIFO(UART0), '\n');
					system_os_post(TASK0_PRIO, UART_TASK_SIG, rx_buf->BuffState);
				} else if (ch < ' ') {
					break;
				}
				*rx_buf->pWritePos++ = ch;
				if (rx_buf->pWritePos == rx_buf->pRcvMsgBuff + rx_buf->RcvBuffSize)
					rx_buf->pWritePos = rx_buf->pRcvMsgBuff;
//				WRITE_PERI_REG(UART_FIFO(UART0), ch);
			}
		} while (false);
	}
	WRITE_PERI_REG(UART_INT_CLR(UART0), UART_RXFIFO_FULL_INT_CLR);
}

void rx_line_read(RcvMsgBuff *rx_buf, uint8 *buffer) {
	int i = 0;

	ETS_UART_INTR_DISABLE();

	do {
		if (rx_buf->BuffState == 0)
			break;
		uint8 ch;
		while (rx_buf->pReadPos != rx_buf->pWritePos) {
			ch = *rx_buf->pReadPos;
			buffer[i] = ch;
			rx_buf->pReadPos++;
			if (rx_buf->pReadPos == rx_buf->pRcvMsgBuff + rx_buf->RcvBuffSize)
				rx_buf->pReadPos = rx_buf->pRcvMsgBuff;
			if (ch == '\r') {
				rx_buf->BuffState--;
				break;
			}
			i++;
		}
		if (ch == '\r')
			break;
		i = 0;
	} while (false);

	ETS_UART_INTR_ENABLE();

	buffer[i] = 0;
	return;
}

bool uart_task_cb(os_event_t *e) {
	if (e->sig != UART_TASK_SIG)
		return false;
	uint8 data[256];
	rx_line_read(&UartDev.rcv_buff, data);
	DBG("%s", data);
	if (uart_cb != NULL)
		uart_cb(data);
	return true;
}

void ICACHE_FLASH_ATTR uart_init(uart_cb_t cb) {
	uart_cb = cb;
	ets_isr_attach(ETS_UART_INUM, uart_intr_handler, (void*) &UartDev.rcv_buff);
	UartDev.rcv_buff.pWritePos = UartDev.rcv_buff.pRcvMsgBuff;
	UartDev.rcv_buff.pReadPos = UartDev.rcv_buff.pRcvMsgBuff;
	UartDev.rcv_buff.BuffState = 0;
	WRITE_PERI_REG(UART_INT_ENA(UART0), UART_RXFIFO_FULL_INT_ENA);
	ETS_UART_INTR_ENABLE();
}
