/*
 * main.c
 *
 *  Created on: 22 окт. 2020 г.
 *      Author: user
 */

#include <user_interface.h>
#include <osapi.h>

#include "driver/uart.h"

os_event_t task0_queue[TASK0_QUEUE_LEN];

void uart_cb_func(uint8 *data) {
	DBG("\"%s\"", data);
}

void ICACHE_FLASH_ATTR task0_cb(os_event_t *e) {
	if (uart_task_cb(e))
		return;
}

void ICACHE_FLASH_ATTR start(void) {
	DBG("%s", "Start!");
	system_os_task(task0_cb, TASK0_PRIO, task0_queue, TASK0_QUEUE_LEN);
	wifi_set_opmode_current(NULL_MODE);
}

void ICACHE_FLASH_ATTR main(void) {
	DBG("%s", "Start!");
	os_printf("\nFW version:  %d.%d.%d build %d\n\n", VERSION >> 28, ( VERSION >> 23) & 0x1F, ( VERSION >> 14) & 0x1FF,
	VERSION & 0x3FFF);
	uart_init(uart_cb_func);
}
