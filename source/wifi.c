/*
 * wifi.c
 *
 *  Created on: 25 окт. 2020 г.
 *      Author: Lord
 */

#include <user_interface.h>
#include <osapi.h>
#include <mem.h>

#include "params.h"
#include "wifi.h"

bool sta_connected = false;
os_timer_t sta_tm;

void ICACHE_FLASH_ATTR sta_tm_cb(void *arg) {
	wifi_station_connect();
}

void ICACHE_FLASH_ATTR wifi_event_handler(System_Event_t *event) {
	switch (event->event) {
	case EVENT_STAMODE_CONNECTED:
		os_printf("%s\r\n", "EVENT_STAMODE_CONNECTED");
		break;
	case EVENT_STAMODE_DISCONNECTED:
		os_printf("%s\r\n", "EVENT_STAMODE_DISCONNECTED");
		if (sta_connected) {
			sta_connected = false;
			wifi_station_connect();
		} else {
			if (wifi_get_opmode() & STATION_MODE != 0)
				os_timer_arm(&sta_tm, params.sta_reconnect_time * 1000, false);
			wifi_set_opmode_current(wifi_get_opmode() | SOFTAP_MODE);
		}
		break;
	case EVENT_STAMODE_AUTHMODE_CHANGE:
		os_printf("%s\r\n", "EVENT_STAMODE_AUTHMODE_CHANGE");
		break;
	case EVENT_STAMODE_GOT_IP:
		os_printf("%s\r\n", "EVENT_STAMODE_GOT_IP");
		sta_connected = STATION_MODE;
		break;
	case EVENT_STAMODE_DHCP_TIMEOUT:
		os_printf("%s\r\n", "EVENT_STAMODE_DHCP_TIMEOUT");
		break;
	case EVENT_SOFTAPMODE_STACONNECTED:
		os_printf("%s\r\n", "EVENT_SOFTAPMODE_STACONNECTED");
		break;
	case EVENT_SOFTAPMODE_STADISCONNECTED:
		os_printf("%s\r\n", "EVENT_SOFTAPMODE_STADISCONNECTED");
		break;
	case EVENT_SOFTAPMODE_PROBEREQRECVED:
		os_printf("%s\r\n", "EVENT_SOFTAPMODE_PROBEREQRECVED");
		break;
	case EVENT_OPMODE_CHANGED:
		os_printf("%s\r\n", "EVENT_OPMODE_CHANGED");
		break;
	case EVENT_SOFTAPMODE_DISTRIBUTE_STA_IP:
		os_printf("%s\r\n", "EVENT_SOFTAPMODE_DISTRIBUTE_STA_IP");
		break;
	}
}

void ICACHE_FLASH_ATTR wifi_init_def(void) {
	wifi_set_opmode_current(STATIONAP_MODE);

	struct station_config *sta_conf = (struct station_config *) os_zalloc(sizeof(struct station_config));
	os_strncpy(sta_conf->ssid, STA_SSID, 32);
	os_strncpy(sta_conf->password, STA_PASS, 64);
	wifi_station_set_config(sta_conf);
	os_free(sta_conf);

	struct softap_config *ap_conf = (struct softap_config *) os_zalloc(sizeof(struct softap_config));
	os_strncpy(ap_conf->ssid, AP_SSID, 32);
	ap_conf->ssid_len = os_strlen(ap_conf->ssid);
	if (ap_conf->ssid_len > 32)
		ap_conf->ssid_len = 32;
	os_strncpy(ap_conf->password, AP_PASS, 64);
	ap_conf->channel = 7;
	ap_conf->max_connection = 4;
	ap_conf->authmode = AUTH_WPA2_PSK;
	ap_conf->beacon_interval = 100;
	wifi_softap_set_config(ap_conf);
	os_free(ap_conf);

	wifi_station_set_auto_connect(1);

	wifi_set_opmode(WIFI_MODE);

	params.sta_reconnect_time = STA_RECONNECT_TIME;
}

void ICACHE_FLASH_ATTR wifi_init(void) {
	os_timer_disarm(&sta_tm);
	os_timer_setfn(&sta_tm, sta_tm_cb, 0);

	wifi_set_event_handler_cb(wifi_event_handler);

	if (first_load)
		wifi_init_def();

	wifi_station_set_reconnect_policy(false);
}
