/*
 * params.c
 *
 *  Created on: 25 окт. 2020 г.
 *      Author: Lord
 */

#include <user_interface.h>

#include "version.h"
#include "params.h"

bool first_load = false;
params_t params;

void ICACHE_FLASH_ATTR params_set_def(void) {
	params.version = VERSION;
}

STATUS ICACHE_FLASH_ATTR params_store(void) {
	if (system_param_save_with_protect(PARAM_STORE_SEC, &params, sizeof(params)))
		return OK;
	return FAIL;
}

STATUS ICACHE_FLASH_ATTR params_load(void) {
	if (!system_param_load(PARAM_STORE_SEC, 0, &params, sizeof(params))) {
		return FAIL;
	}
	if (params.version != VERSION) {
		first_load = true;
		params_set_def();
		return params_store();
	}
	return OK;
}
