/*
 * user_config.h
 *
 *  Copied from hello world demo on: 22 окт. 2020 г.
 *      Lord
 */

#ifndef USER_CONFIG_H
#define USER_CONFIG_H

#define PARAM_STORE_SEC 248

#define TASK0_PRIO 0
#define TASK0_QUEUE_LEN 32

#define WIFI_MODE STATION_MODE
//#define STA_SSID "home-point"
//#define STA_PASS "LittleBigUnicorn"
#define STA_SSID "RVP"
#define STA_PASS "rvp1234567"
#define STA_RECONNECT_TIME 10

#define AP_SSID "RimMQTT"
#define AP_PASS "12345678"

#define HOSTNAME "RimMQTT"

#ifndef VERSION
#define VERSION 0x00
#endif

#define DEBUG

#ifdef DEBUG
#include <osapi.h>
#define DBG(s, ...) os_printf("%s(): " s "\n", __func__, ##__VA_ARGS__)
#else
#define DBG(s, ...)
#endif

#endif /* USER_CONFIG_H */
