/*
 * params.h
 *
 *  Created on: 25 окт. 2020 г.
 *      Author: Lord
 */

#ifndef INCLUDE_PARAMS_H
#define INCLUDE_PARAMS_H

typedef struct params_t {
	uint32 version;
	uint8 sta_reconnect_time;
} params_t;

extern bool first_load;
extern params_t params;

#endif /* INCLUDE_PARAMS_H */
