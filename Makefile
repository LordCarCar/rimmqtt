MakePath = C:/Espressif/examples/ESP8266
VERSIONER 	= C:\Espressif\Versioner.exe
VERSION = $(shell $(VERSIONER))


# Main settings includes
include	$(MakePath)/settings.mk

# Individual project settings (Optional)
BOOT		= new
APP			= 1
#SPI_SPEED	= 40
#SPI_MODE	= QIO
SPI_SIZE_MAP	= 6
ESPPORT		= COM3
ESPBAUD		= 2000000

# Basic project settings
MODULES	= driver source system
LIBS	= c gcc hal phy pp net80211 lwip wpa main crypto

# Root includes
include	$(MakePath)/common_nonos.mk
